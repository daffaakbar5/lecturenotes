import React from "react";
import Link from "next/link";

const Card = ({ datas }) => {
  return (
    <Link href={`/notes/${datas.id}`}>
      <div className="shadow-xl card bg-base-100 cursor-pointer">
        {/* <figure>
          <img
            src="https://api.lorem.space/image/shoes?w=400&h=225"
            alt="Shoes"
          />
        </figure> */}
        <div className="card-body">
          <h2 className="card-title">
            {datas.title}
            {/* <div className="badge badge-secondary">NEW</div> */}
          </h2>
          <p>{datas.description}</p>
          <div className="justify-end card-actions">
            <div className="badge badge-outline">featuretagcomingsoon</div>
            {/* <div className="badge badge-outline">Products</div> */}
          </div>
        </div>
      </div>
    </Link>
  );
};

export default Card;
