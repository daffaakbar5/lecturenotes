import React, { useState, useEffect } from "react";
import Link from "next/link";
import Navbar from "../Navbar";
import { auth, db } from "../../config/firebase";
import { onAuthStateChanged } from "firebase/auth";

const Drawer = ({ children }) => {
  const [user, setUser] = useState(false);
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setUser(true);
      }
    });
  });

  return (
    <div className="drawer ">
      <input id="my-drawer" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content ">
        {/* Page content here  */}
        {/* <Navbar /> */}
        <main>{children}</main>
      </div>
      <div className="drawer-side ">
        <label htmlFor="my-drawer" className="drawer-overlay "></label>
        <ul className="p-4 overflow-y-auto rounded-br-full menu w-80 bg-base-100 text-base-content rounded-tr-3xl">
          {/* Sidebar content here  */}
          <a className="px-2 py-1 bg-white shadow-sm rounded-3xl hover:shadow-xl">
            <img src="/LogoLN.svg" alt="" width={180} />
          </a>
          <hr className="mb-2" />
          <li>
            <Link href={"/"}>
              <a>👩‍🎓 Home</a>
            </Link>
          </li>
          {user ? (
            <>
              <li>
                <Link href={"/dashboard"}>
                  <a>📚 Dashboard</a>
                </Link>
              </li>
              <li>
                <Link href={"/addnotes"}>
                  <a>✍ Add Notes</a>
                </Link>
              </li>
            </>
          ) : (
            ""
          )}

          <li className="">
            <Link href={"/login"}>
              <a>🔒 Login</a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Drawer;
