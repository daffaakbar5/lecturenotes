import React from "react";
import { getAuth, signOut } from "firebase/auth";
import { auth } from "../../config/firebase";
import { useRouter } from "next/router";

const Navbar = () => {
  const router = useRouter();

  const handleLogout = () => {
    signOut(auth)
      .then(() => {
        // Sign-out successful.
        alert("Anda berhasil Logout!");
        router.push("/login");
      })
      .catch((error) => {
        // An error happened.
        console.log(error);
      });
  };
  return (
    <div className="w-full bg-accent">
      <div className="container mx-auto navbar ">
        <div className="flex-1 ">
          <a className="px-4 py-1 bg-white shadow-lg rounded-3xl hover:shadow-xl hover:bg-gray-100">
            <img src="/LogoLN.svg" alt="" width={180} />
          </a>
        </div>
        <div className="flex-none mr-14">
          <div className="dropdown dropdown-end ">
            <label tabIndex="0" className="btn btn-ghost btn-circle avatar">
              <div className="w-10 rounded-full">
                <img src="/SVG/iconperson.svg" />
              </div>
            </label>
            <ul
              tabIndex="0"
              className="p-2 mt-3 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52"
            >
              <li>
                <a className="justify-between">
                  Profile
                  <span className="text-center bg-gray-300 badge badge-sm">
                    coming soon
                  </span>
                </a>
              </li>
              {/* <li>
                <a>Settings</a>
              </li> */}
              <li>
                <a onClick={handleLogout}>Logout</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="fixed right-10">
          <label
            className="btn btn-circle btn-primary swap swap-rotate "
            htmlFor="my-drawer"
          >
            <input type="checkbox" />
            <svg
              className="fill-current swap-off"
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 512 512"
            >
              <path d="M64,384H448V341.33H64Zm0-106.67H448V234.67H64ZM64,128v42.67H448V128Z" />
            </svg>
            <svg
              className="fill-current swap-on"
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 512 512"
            >
              <polygon points="400 145.49 366.51 112 256 222.51 145.49 112 112 145.49 222.51 256 112 366.51 145.49 400 256 289.49 366.51 400 400 366.51 289.49 256 400 145.49" />
            </svg>
          </label>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
