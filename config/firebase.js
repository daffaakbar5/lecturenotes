// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBsMLDKEMtQZV21um5eilXfI2vWE-9Yn5M",
  authDomain: "lecturenotes-6ca56.firebaseapp.com",
  projectId: "lecturenotes-6ca56",
  storageBucket: "lecturenotes-6ca56.appspot.com",
  messagingSenderId: "1049551803697",
  appId: "1:1049551803697:web:7b8e7462fefdb1aef50c10",
  measurementId: "G-QP0YP7DS4C",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
export const auth = getAuth(app);
export const db = getFirestore(app);

// export { app, analytics, auth };
