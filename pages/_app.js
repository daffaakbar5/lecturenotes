import "../styles/globals.css";
import Script from "next/script";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Script
        src="https://cdn.tiny.cloud/1/08u9cxjorb9063vllo7juugz6ltrwls92ba1jjhtrjhzhie0/tinymce/6/tinymce.min.js"
        referrerpolicy="origin"
      ></Script>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
