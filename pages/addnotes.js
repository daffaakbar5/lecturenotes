import React, { useState, useRef, useEffect } from "react";
import { Card, Drawer, Navbar } from "../components";
import { Editor } from "@tinymce/tinymce-react";
import { auth, db } from "../config/firebase";
import { onAuthStateChanged } from "firebase/auth";
import { useRouter } from "next/router";
import {
  doc,
  setDoc,
  addDoc,
  collection,
  serverTimestamp,
} from "firebase/firestore";
import moment from "moment";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export default function Addnotes() {
  const [form, setForm] = useState({ title: "", description: "", note: "" });
  const [currentDate, setCurrentDate] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    let date = moment().format();
    setCurrentDate(date);
  }, []);
  const MySwal = withReactContent(Swal);
  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    // alert(JSON.stringify(form));
    await addDoc(collection(db, "notes"), {
      uid: auth.currentUser.uid,
      email: auth.currentUser.email,
      title: form.title,
      description: form.description,
      note: form.note,
      timestamp: serverTimestamp(),
      time: currentDate,
    })
      .then((res) => {
        // console.log(res);

        // alert("Data Berhasil ditambahkan!");
        Swal.fire({
          icon: "success",
          title: "Your note has been saved",
        });
        // console.log(form);
        // setForm({ title: "", description: "", note: "" });
        // console.log(form);
      })
      .catch((err) => {
        console.log(err);
      });
    // setForm({ title: "", description: "", note: "" });
    // setForm("");
    // console.log(form);
    setIsLoading(false);
    router.push("/dashboard");
  };

  const router = useRouter();
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;
        // ...
        // alert(uid);
        // router.push("/dashboard");
      } else {
        router.push("/login");
      }
      // console.log(form);
    });
  }, []);
  // add new document

  return (
    <Drawer>
      <Navbar />
      <div className="container mx-auto">
        <div className="my-5 bg-slate-50">
          <form className="px-5 mt-5 shadow-xl " onSubmit={handleSubmit}>
            <h1 className="pt-4 text-4xl font-bold text-center text-red-600 ">
              Add Notes
            </h1>
            <div className="w-full form-control ">
              <label className="label">
                <span className="text-xl text-red-600 ">Title :</span>
              </label>
              <input
                type="text"
                placeholder="title here ..."
                className="w-1/2 input input-bordered"
                onChange={(e) => setForm({ ...form, title: e.target.value })}
                required
              />
            </div>
            <div className="w-full form-control ">
              <label className="label">
                <span className="text-xl text-red-600 ">
                  Short description :
                </span>
              </label>
              <input
                type="text"
                placeholder="Short description ..."
                className="input input-bordered "
                onChange={(e) =>
                  setForm({ ...form, description: e.target.value })
                }
                required
              />
            </div>
            <div className="w-full form-control">
              <label className="label">
                <span className="text-xl text-red-600">Your notes :</span>
              </label>
              <Editor
                initialValue={"Write notes here ..."}
                onEditorChange={(e) => setForm({ ...form, note: e })}
                id="08u9cxjorb9063vllo7juugz6ltrwls92ba1jjhtrjhzhie0"
                apiKey="08u9cxjorb9063vllo7juugz6ltrwls92ba1jjhtrjhzhie0"
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist",
                    "autolink",
                    "lists",
                    "link",
                    "image",
                    "charmap",
                    "anchor",
                    "searchreplace",
                    "visualblocks",
                    "code",
                    "fullscreen",
                    "insertdatetime",
                    "media",
                    "table",
                    "preview",
                    "help",
                    "wordcount",
                  ],
                  toolbar:
                    "undo redo | blocks | " +
                    "bold italic forecolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
              />
              {/* <button onClick={log}>Log editor content</button> */}
            </div>
            <div className="w-full pb-5 my-5 form-control ">
              {isLoading ? (
                <button className="btn loading btn-primary">loading</button>
              ) : (
                <input
                  type="submit"
                  value="Submit"
                  className=" btn btn-primary"
                />
              )}
            </div>
          </form>
        </div>
      </div>
    </Drawer>
  );
}
