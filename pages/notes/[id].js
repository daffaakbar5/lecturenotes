import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { auth, db } from "../../config/firebase";
import { onAuthStateChanged } from "firebase/auth";
import {
  collection,
  query,
  where,
  getDocs,
  onSnapshot,
  doc,
  deleteDoc,
} from "firebase/firestore";
import { Drawer, Navbar } from "../../components";
import moment from "moment";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export default function NoteDetails() {
  const router = useRouter();
  const { id } = router.query;
  const [dataNote, setDataNote] = useState([]);
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        onSnapshot(doc(db, "notes", id), (doc) => {
          console.log("Current data: ", doc.data());
          // setDataNote(doc.data());

          if (doc.data() !== undefined) {
            setDataNote({
              title: doc.data().title,
              description: doc.data().description,
              note: doc.data().note,
              timestamp: doc.data().timestamp,
              time: moment(doc.data().time).calendar(),
            });
          }
        });
      } else {
        router.push("/login");
      }
    });
  }, []);

  //   console.log(dataNote);
  //   console.log("ini ", dataNote);
  const MySwal = withReactContent(Swal);

  const handleDelete = () => {
    // confirm(`Yakin menghapus ${dataNote.title} ?`);
    // await deleteDoc(doc(db, "notes", id))
    //   .then((res) => {
    //     // alert("Data berhasil di hapus");
    //     router.push("/dashboard");
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await deleteDoc(doc(db, "notes", id))
          .then((res) => {
            // alert("Data berhasil di hapus");
            router.push("/dashboard");
            console.log(res);
          })
          .catch((err) => {
            console.log(err);
          });
        Swal.fire("Deleted!", "Your note has been deleted.", "success");
        router.push("/dashboard");
      }
    });
    // alert(cityRef);
  };
  return (
    <Drawer>
      <Navbar />
      <div className="flex justify-center h-screen bg-gray-50 ">
        <div className="container p-10 mx-auto mt-10 rounded-lg ">
          <div className="relative shadow-xl bg-base-100 rounded-2xl">
            <span className="absolute z-10 py-3 text-lg badge badge-primary -top-3 -right-3 ">
              {dataNote.time}
            </span>
            <span
              className="absolute z-10 cursor-pointer text-base py-3 badge-error badge -bottom-3 -right-3 "
              onClick={handleDelete}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-trash mr-1 "
                viewBox="0 0 16 16"
              >
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                <path
                  fillRule="evenodd"
                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                />
              </svg>
              {"   "}
              Delete
            </span>

            {/* <figure>
              <img src="" alt="Shoes" />
            </figure> */}
            <div className="card-body">
              <h2 className="text-4xl card-title">
                {dataNote.title}

                {/* <div className="badge badge-secondary">NEW</div> */}
              </h2>
              <p className="text-gray-400 ">{dataNote.description}</p>
              <div
                className="my-5"
                dangerouslySetInnerHTML={{ __html: dataNote.note }}
              >
                {/* {dataNote.description} */}
              </div>
              <div className="justify-end card-actions">
                {/* <div className="badge badge-outline">Fashion</div> */}
                <div className="badge badge-outline">featuretagcomingsoon</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Drawer>
  );
}
