import React, { useState, useEffect } from "react";
import Link from "next/link";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../config/firebase";
import {
  GoogleAuthProvider,
  signInWithPopup,
  onAuthStateChanged,
  signInWithRedirect,
} from "firebase/auth";
import {
  doc,
  setDoc,
  addDoc,
  collection,
  serverTimestamp,
} from "firebase/firestore";
import { db } from "../config/firebase";
import { useRouter } from "next/router";

export default function Signup() {
  const [form, setForm] = useState({
    fullName: "",
    email: "",
    password: "",
    repassword: "",
  });
  const router = useRouter();
  const handleSubmit = async (event) => {
    event.preventDefault();

    // alert(JSON.stringify(form));
    if (
      form.password != null &&
      form.password == form.repassword &&
      form.password.length >= 6
    ) {
      await createUserWithEmailAndPassword(auth, form.email, form.password)
        .then(async (userCredential) => {
          // Signed in
          const user = userCredential.user;
          console.log("ini isi user? ", user);
          await addDoc(collection(db, "user"), {
            uid: user.uid,
            email: user.email,
            fullName: form.fullName,
            timestamp: serverTimestamp(),
          })
            .then((res) => {
              console.log("ini res?", res);
              router.push("/dashboard");

              // alert("Data Berhasil ditambahkan!");
              // console.log(form);
              // setForm({ title: "", description: "", note: "" });
              // console.log(form);
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(error);
          alert(errorMessage);
        });
    } else {
      alert("Password invalid, check your password!");
    }
  };
  const provider = new GoogleAuthProvider();
  const googleSignin = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // console.log(user);
        // ...
        addDoc(collection(db, "user"), {
          uid: user.uid,
          email: user.email,
          fullName: user.displayName,
          timestamp: serverTimestamp(),
          profilepicture: user.photoURL,
        })
          .then((res) => {
            console.log("ini res?", res);

            alert(`Selamat datang `, user.displayName);
            // console.log(form);
            // setForm({ title: "", description: "", note: "" });
            // console.log(form);
            router.push("/dashboard");
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
        // ...
        console.log(errorCode);
        console.log(errorMessage);
        console.log(email);
        console.log(credential);
      });
  };
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;
        // ...
        // alert(uid);
        router.push("/dashboard");
      } else {
        // router.push("/register");
      }
    });
  }, []);

  return (
    <div className="h-screen md:flex">
      <div className="relative overflow-hidden md:flex w-1/2 bg-[url('/study.jpg')] bg-contain bg-no-repeat  justify-around items-center hidden ">
        <div className="w-1/2 p-10 bg-gray-200 bg-opacity-70 rounded-3xl">
          <h1 className="font-sans text-4xl font-bold text-center text-red-700 uppercase ">
            lecture notes
          </h1>
          <p className="mt-1 text-justify text-indigo-80">
            LectureNotes intends to pioneer the notes making ecosystem, the peer
            learning modules, and most importantly remote access.
          </p>
          <Link href={"/"}>
            <button
              type="submit"
              className="block w-full mt-4 mb-2 font-bold btn btn-primary rounded-2xl"
            >
              Read More
            </button>
          </Link>
        </div>
        <div className="absolute border-4 border-t-8 rounded-full -bottom-32 -left-40 w-80 h-80 border-opacity-30"></div>
        <div className="absolute border-4 border-t-8 rounded-full -bottom-40 -left-20 w-80 h-80 border-opacity-30"></div>
        <div className="absolute border-4 border-t-8 rounded-full -top-40 -right-0 w-80 h-80 border-opacity-30"></div>
        <div className="absolute border-4 border-t-8 rounded-full -top-20 -right-20 w-80 h-80 border-opacity-30"></div>
      </div>
      <div className="flex items-center justify-center py-10 md:w-1/2 bg-primary">
        <div className="p-16 bg-white rounded-3xl">
          <form onSubmit={handleSubmit}>
            <h1 className="mb-2 text-2xl font-bold text-gray-800">
              Welcome to Lecture Notes!
            </h1>
            <p className="text-sm font-normal text-gray-600 mb-7">
              Please register first!
            </p>
            {/* <p className="text-sm font-normal text-gray-600 mb-7">Welcome Back</p> */}
            <div className="flex items-center px-3 py-2 mb-4 border-2 rounded-2xl">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 text-gray-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                  clipRule="evenodd"
                />
              </svg>
              <input
                className="pl-2 border-none outline-none"
                type="text"
                name=""
                id=""
                placeholder="Full name"
                onChange={(e) => {
                  setForm({ ...form, fullName: e.target.value });
                }}
              />
            </div>
            <div className="flex items-center px-3 py-2 mb-4 border-2 rounded-2xl">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 text-gray-400"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                />
              </svg>
              <input
                className="pl-2 border-none outline-none"
                type="email"
                name=""
                id=""
                placeholder="Email Address"
                onChange={(e) => {
                  setForm({ ...form, email: e.target.value });
                }}
              />
            </div>
            <div className="flex items-center px-3 py-2 mb-4 border-2 rounded-2xl">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 text-gray-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                  clipRule="evenodd"
                />
              </svg>
              <input
                className="pl-2 border-none outline-none"
                type="password"
                name=""
                id=""
                placeholder="Password"
                onChange={(e) => {
                  setForm({ ...form, password: e.target.value });
                }}
              />
            </div>
            <div className="flex items-center px-3 py-2 border-2 rounded-2xl">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-5 h-5 text-gray-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                  clipRule="evenodd"
                />
              </svg>
              <input
                className="pl-2 border-none outline-none"
                type="Password"
                name=""
                id=""
                placeholder="Re-type password"
                onChange={(e) => {
                  setForm({ ...form, repassword: e.target.value });
                }}
              />
            </div>

            <button
              type="submit"
              className="block w-full py-2 mt-4 mb-2 font-semibold btn btn-primary rounded-2xl"
            >
              Sign up
            </button>
          </form>
          <span className="ml-2 text-sm cursor-pointer">
            Have account?
            <Link href={"/login"}>
              <a className="ml-2 font-bold text-purple-500 hover:text-purple-800">
                Login
              </a>
            </Link>
          </span>
          <br />
          <p className="my-2 text-center ">Or</p>
          <span className="flex items-center justify-center ml-2 text-sm cursor-pointer">
            Sign up with
            <button
              className="px-2 py-2 ml-2 font-bold shadow-md rounded-2xl hover:shadow-lg"
              onClick={googleSignin}
            >
              <img src="/google.png" alt="" width={20} />
            </button>
          </span>
        </div>
      </div>
    </div>
  );
}
