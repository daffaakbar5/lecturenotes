import Head from "next/head";
import Image from "next/image";

import { Drawer } from "../components";

export default function Home() {
  return (
    <Drawer>
      <div className="hero min-h-screen bg-[url('https://cdn.pixabay.com/photo/2017/04/05/08/28/consulting-2204253_960_720.png')]">
        <div className="hero-overlay bg-opacity-70"></div>
        <div className="text-center hero-content text-neutral-content">
          <div className="max-w-lg ">
            <h1 className="mb-5 text-5xl font-bold uppercase md:text-8xl text-accent ">
              Lecture Notes
            </h1>
            <p className="px-2 py-2 mb-5 bg-gray-100 rounded-3xl">
              LectureNotes intends to pioneer the notes making ecosystem, the
              peer learning modules, and most importantly remote access.
              <br />
            </p>
            <label
              htmlFor="my-drawer"
              className="w-full btn btn-primary drawer-button"
            >
              👩‍🎓 Click
            </label>
          </div>
        </div>
        <footer className="footer bottom-0 absolute p-4 bg-neutral justify-center text-neutral-content">
          <div className="items-center grid-flow-col">
            <img src="/LogoLN.svg" alt="" width={130} />
            <p>Copyright © 2022 by Daffa Akbar - Build with ❤ </p>
          </div>
        </footer>
      </div>
    </Drawer>
  );
}
