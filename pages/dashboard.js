import React, { useState, useEffect } from "react";
import { Card, Drawer, Navbar } from "../components";
import { auth, db } from "../config/firebase";
import { onAuthStateChanged } from "firebase/auth";
import { useRouter } from "next/router";
import {
  collection,
  query,
  where,
  getDocs,
  onSnapshot,
  orderBy,
} from "firebase/firestore";

export default function Dashboard() {
  const [dataNotes, setDataNotes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [dataReady, setDataReady] = useState(true);
  const router = useRouter();
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;
        // ...
        // alert(uid);
        // router.push("/dashboard");
        const q = query(
          collection(db, "notes"),
          where("uid", "==", uid),
          orderBy("time", "desc")
        );
        // console.log(q);
        onSnapshot(q, (querySnapshot) => {
          const dataNote = [];
          querySnapshot.forEach((doc) => {
            // console.log(doc.id);
            dataNote.push({
              id: doc.id,
              title: doc.data().title,
              description: doc.data().description,
              note: doc.data().note,
              time: doc.data().time,
            });
          });
          // console.log("Current cities in CA: ", dataNote.join(", "));
          setDataNotes(dataNote);
          setLoading(true);
          setDataReady(dataNote.length);
        });
      } else {
        router.push("/login");
      }
    });
  }, []);
  // console.log(dataNotes);

  return (
    <Drawer>
      <Navbar />
      <div className="flex items-center justify-center bg-primary">
        <div className="container p-8 mx-auto mt-5 rounded-lg">
          {/* <form> */}
          <h1 className="mb-5 text-4xl font-bold text-center text-white">
            Find your notes
          </h1>
          <div className="items-center justify-between px-2 py-1 overflow-hidden bg-white rounded-lg sm:flex ">
            <input
              className="flex-grow px-8 py-3 text-base text-gray-600 outline-none"
              type="text"
              placeholder="Search..."
              onChange={(e) => {
                setSearchTerm(e.target.value);
              }}
            />
            <div className="relative items-center px-2 mx-auto space-x-4 rounded-lg ms:flex ">
              {/* <select
                  id="Com"
                  className="px-4 py-2 text-base text-gray-800 border-2 rounded-lg outline-none"
                >
                  <option value="com" selected>
                    com
                  </option>
                  <option value="net">net</option>
                  <option value="org">org</option>
                  <option value="io">io</option>
                </select> */}
              {/* <button className="w-full px-8 py-3 text-white rounded-lg btn-neutral btn ">
                Search
              </button> */}
            </div>
          </div>
          {/* </form> */}
        </div>
      </div>
      {loading ? (
        <div className="relative grid grid-cols-1 gap-5 p-10 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4">
          {dataNotes
            .filter((note) => {
              if (searchTerm === "") {
                return note;
              } else if (
                note.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
                note.description
                  .toLowerCase()
                  .includes(searchTerm.toLowerCase())
              ) {
                return note;
              }
            })
            .map((note, index) => {
              return <Card datas={note} key={index} />;
            })}
        </div>
      ) : (
        <div className="text-center ">
          <h1 className="mt-10 text-3xl font-semibold text-center capitalize loading btn btn-primary">
            Loading...
          </h1>
        </div>
      )}
      {dataReady === 0 ? (
        <div className="text-center ">
          <h1 className="mt-10 text-3xl font-semibold text-center capitalize btn btn-primary">
            data kosong...
          </h1>
        </div>
      ) : (
        ""
      )}
    </Drawer>
  );
}
